+++
title = "#replacefacebook"

date = 2018-04-08
sort_by = "date"
tags = ["privacy","facebook","social"]
slug = "replacefacebook"

template = "page.html"
+++

Over the past week or so, Facebook has gotten into a lot of trouble.
#deletefacebook has been trending, riding on the back of John Biggs'
[scathing article of the same title](https://techcrunch.com/2018/03/19/deletefacebook/).
People finally seem ready to jump ship from Facebook, after years of privacy
advocates telling them they should. It has been wonderful to think about the
prospect of every abused user (or as Richard Stallman would say, [useds](
https://stallman.org/glossary.html)) abandoning Facebook and living a
better life for it, that might not be reality.  Facebook, for all its faults,
has become something of a resource.  People use Facebook and Facebook products,
like [WhatsApp](https://www.bloomberg.com/news/articles/2014-10-28/facebook-s-22-billion-whatsapp-deal-buys-10-million-in-sales) and
[Instagram](
https://dealbook.nytimes.com/2012/04/09/facebook-buys-instagram-for-1-billion/), every
day.

Facebook itself is powerful in that it helps you keep in touch with the people
you care for in an integrated way. That may not be worth the numerous privacy
violations. These violations include:

* [Keeping "shadow profiles" on users](https://www.dailydot.com/news/facebook-shadow-profiles-privacy-faq/):

> **Who has a shadow profile? Are they real?**
>
> Well, potentially everyone who has a Facebook account. They contain a certain amount
> of information you’re not surprised Facebook knows about you—your name, your interests,
> your relationship status, how many times you’ve liked your friends posts. But at the same
> time, Facebook’s been able to smartly collect other data about you. **Even if you’ve never
> told Facebook your phone number, for instance, it might have it.** As well as your second
> and third and fourth email addresses.
>
> [...]
>
> Max Schrems, owner of Europe vs. Facebook and privacy rights advocate, launched a
> complaint against Facebook’s European offices, headquartered in Ireland, citing seven
> different instances where shadow profiles potentially violate the country’s Data
> Protection Act (read the PDF here). **Schrems asserts that the profiles gathered
> “excessive amounts of information about data subjects without notice or consent by the
> data subject. In many cases these information might be embarrassing or intimidating for
> the data subject.”**

* [Logging text messages and calls from your
phone](https://www.theguardian.com/technology/2018/mar/25/facebook-logs-texts-and-calls-users-find-as-they-delete-accounts-cambridge-analytica):

> As users continue to delete their Facebook accounts in the wake of the Cambridge
> Analytica scandal, a number are discovering that the social network holds far more
> data about them than they expected, including **complete logs of incoming and outgoing
> calls and SMS messages.**

* [Policy requiring users to provide
a real name](https://www.irishtimes.com/news/ireland/irish-news/protest-at-dublin-facebook-hq-over-real-name-policy-1.2345414)

* ["VPN" app that logged all connections](https://gizmodo.com/do-not-i-repeat-do-not-download-onavo-facebook-s-vam-1822937825):

> VPNs work by forcing your laptop or mobile device to establish a connection
> to a third-party server before then connecting you to any websites or online services.
> Using an encrypted tunnel, a VPN can prevent your broadband or wireless provider (AT&T,
> Comcast, et al) from keeping track of the websites you visit. What’s hore, a VPN service
> can mask your own IP address from those websites, helping you to traverse the net without
> surrendering locational data. [...] Facebook, however, purchased Onavo from an Israeli firm
> in 2013 for an entirely different reason, as described in a Wall Street Journal report last
> summer. **The company is actually collecting and analyzing the data of Onavo users. Doing so
> allows Facebook to monitor the online habits of people outside their use of the Facebook app
> itself. For instance, this gave the company insight into Snapchat’s dwindling user base,
> even before the company announced a period of diminished growth last year.**

In addition to all of these violations, [the revelations about Cambridge Analytica's abuse and
the 2016 elections](https://www.theatlantic.com/technology/archive/2018/03/the-cambridge-analytica-scandal-in-three-paragraphs/556046/)
has people eager to flee from the scourge that is Facebook. People have been fleeing to different
platforms, like [Mastodon](https://joinmastodon.org/), where users have more control. Mastodon, however,
is a microbloggig platform more akin to Twitter. The community is wonderful and the platform is
one of the most mature of its kind, but it's not what I imagine most Facebook  users would look for.
With that in mind, what _can_ one do to replace Facebook?

The reality is that **people will not leave Facebook unless they can find a way to replace its function**.
This brings up the question: what exactly is Facebook's fucntion? Originally, this post was intended
to be a list of all the platforms that functionally can replace Facebook. In trying them though,
I have found that all of them lack something that Facebook had going for it. Certain aspects
might be great, but as a whole the platform felt inadequate. Eventually I figured it out. It's not just
one magic element that Facebook had, but three core components:

* Community
* Messaging
* Discovery

The community section refers to Facebook's ability to bring communities together.
This can revolve around a group chat, a group wall, a public figure or company's page,
or events. Messaging refers to the integration of Messenger with Facebook's community and
the seamlessness of picking up a device and talking to your friends. Lastly, the most critical
component, is discovery. Facebook users can quickly find things that interest them or
interesting people. They can find content that they love, that they can then quickly
share with their friends.

None of the platforms I've tried incorporate all three.

## What Can We Do Now?

As stated above, the platforms I've tried are not functionally equal to Facebook. The good thing is,
none of them are terrible. In the interim between leaving Facebook and replacing it, we can go
to places that all have at least one thing in common: **they aren't data vampires**. For those who don't know
the disciplines required to help replace Facebook, you have the important role of **spreading the word**.
Unlike major platforms like Facebook and Twitter that sell your data to fund
their reach, these platforms are community-run, so they need to be
community-spread. Now, on to what these platforms actually are:

* [Mastodon](http://joinmastodon.org/)
* [Movim](https://movim.eu/)
* [Friendica](https://friendi.ca/)
* [Diaspora](https://diasporafoundation.org/)
* [Hubzilla](https://project.hubzilla.org/page/hubzilla/hubzilla-project)
* [Matrix](https://matrix.org/blog/home/)

The common theme between these platforms is that they are **federated**.
As [this](https://www.eff.org/deeplinks/2011/03/introduction-distributed-social-network)
excellent article from the EFF explains, this means that people can run
and join instances of the platform software and freely communicate
with people from other instances of the platform. Some of these
platforms can even communicate with the others.

The ability to federate is a key part of how we as users, developers,
and _people with agency _can prevent abuse like Facebook's from happening
again. We are not locked in to one instance's rules. We are not subject to
the whims of those who only want to use us for profit. Instead, we have the
power to completely own our data.

Another key that gives us control over corporations that want to abuse us
is the power of **free software** --- and not just free like beer. The
definition that I'm referencing is that of the
[Free Software Foundation and the GNU
Project](https://www.gnu.org/philosophy/free-sw.html):

> A program is free software if the program's users have the four essential
> freedoms:
>
> * The freedom to run the program as you wish, for any purpose (freedom 0).
> * The freedom to study how the program works, and change it so it does your
> computing as you wish (freedom 1). Access to the source code is a precondition
> for this.
> * The freedom to redistribute copies so you can help your
> neighbor (freedom 2).
> * The freedom to distribute copies of your modified versions to others
> (freedom 3). By doing this you can give the whole community a chance to
> benefit from your changes. Access to the source code is a precondition for
> this.

Free software is the best weapon we have against invasions of our privacy.
Can you imagine what would have happened to Facebook if someone could
pop up a copy the moment the company did something shady? Free software gives
users recourse when they need it most.

Any platform meant to replace Facebook **_needs_** to be free and federated,
lest its users fall into the same traps.

## How Do We Actually Replace Facebook?

Replacing the critical components of Facebook is a mountainous task.
Like climbing a mountain it's doable, but it takes hard work and
careful planning. As discussed above, freedom and federation are both
required to avoid creating a Facebook-like monstrosity. This hypothetical
replacement also needs be driven purely by community interest. Knowing the
above, a fully operational replacment will likely come in one of two ways:

* An extension of one or more of the platforms
* Something entirely new

How we as a community should go about this is up to us. In order to have some
semi-centralized places for discussion, I've made set up communities on free
platforms for us to organize, discuss, and implement the tools that will take
down Facebook for good.

* Matrix room: [#replacefacebook:disroot.org](https://matrix.to/#/#replacefacebook:disroot.org)
* XMPP room: replacefacebook@conference.movim.eu
* Friendica Community: [replacefacebook@libranet.de](https://libranet.de/profile/replacefacebook)

I hope to see you outside of Facebook :)

This work is licensed under the [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/2.0/)
