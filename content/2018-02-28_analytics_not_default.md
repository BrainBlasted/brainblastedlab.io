+++
title = "Analytics Should Never Be The Default"
date = 2018-02-28
slug = "dont-assume-analytics"

sort_by = "date"
tags = ["privacy", "analytics"]
description = ""
template = "page.html"
+++

I had an enlighting experience as I was setting up my [last post](/content/2018-02-27_security_under_attack.md).
I am always willing to try out new, cool tech. For this blog,
the new tech chosen was [Gutenberg](https://github.com/Keats/gutenberg),
a static site engine written in the Rust programming language. The base
site is quite barebones, which means that you either find a theme,
or make a theme.

I did **not** feel like rolling my own theme. So, I sought out pre-made themes...
and found there were only two available.

* [Hyde](https://github.com/Keats/hyde) (current theme)
* [Materialize](https://github.com/verpeteren/gutenberg-materialize)

Having a bit of trouble with Hyde last year (likely due to inexperience),
I opted to try out Materialize.

It sucked. Things didn't load properly, the UI design was strange, and
it pulled in JavaScript from different sites. The worst offense,
however, was that Materialize included Google Analytics by default.
I am of the opinion that a blog should not track you. By using Google
Analytics, I would have been commiting a violation of user privacy and
directly contributing to Google's massive database of information.
Considering this is a privacy focused blog, I wasn't having that.

This made an issue in the world wide web apparent to me: content
distributors feel entitled to violate user privacy - whether it be
by a simple blog tracking you or Facebook trying to get every bit of
data they can from you, almost always in the name of  "improving
experience". Privacy is a right, and violation should require consent.
So why do even the simplest of bloggers try to spy on you without consent?

As responsible internet users, content creators,etc., we should never
assume that a user's privacy is ours to violate, and we should never
provide tools that facilitate violations without user consent.

**You may use this content under the terms of the [CC-BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/2.0/) license**.
