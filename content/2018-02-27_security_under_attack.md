+++
title = "You Are Being Used For Your Data"

sort_by = "date"
date = 2018-02-27

tags = ["privacy","security"]
description = "Would you let anyone walk into your house and out with your possessions? Or would you lock the door?"
slug = "used-for-data"

template = "page.html"
+++

A few months ago,
a [data
breach
at
Equifax
](https://www.usatoday.com/story/opinion/2017/09/12/equifax-category-5-data-breach-editorials-debates/657362001/)
exposed millions of Americans' financial information. The breach is haunting,
as anyone who has ever come into contact with Equifax (most Americans)
is at risk. However, what if I were to tell you that there was a threat
that puts the information of every internet user at risk, for much the
same reason?

Hackers are not the biggest risk to your privacy, like they are on TV.
It is highly unlikely that someone will go through the effort
required to hack your computer and actually search for important
data - they'd much rather
[lock it up and get paid in cryptocurrencies](https://en.wikipedia.org/wiki/WannaCry_ransomware_attack).
No, **the biggest risk comes in the form of the data you give away**.

## What Do You Mean Give Away?

The most dangerous data to you is the data you provide when using services like
Google or Facebook.
[Google makes money by selling you](https://www.investopedia.com/articles/investing/020515/business-google.asp).
Their prime business is advertisements. All the cool algorithms they use to
tailor their search results? They are also used to tailor advertisements
specifically toward you, which is highly valuable in an economy about how
many shiny things you can buy.

Google also
[previously scanned your emails in order to "better" advertise
for you](https://www.bloomberg.com/news/articles/2017-06-23/google-will-stop-reading-your-emails-for-gmail-ads),
claiming they refrained from doing so for sensitive emails.
Google is not a private entity, nor are they infallible. Google
[has been](http://www.telegraph.co.uk/news/2016/05/04/millions-of-email-accounts-compromised--in-massive-data-breach-t/)
[affected](https://www.csoonline.com/article/3066841/internet/google-suffers-data-breach-via-benefits-provider.html)
by data breaches before. All it takes is one person being irresonsible
with the right peices of data for this to be exposed, and humans
are often the weakest part of any secure system.

If such a breach were to happen, a lot more than your emails
could be at stake. [This official Google site will
tell you everything that's at risk](https://myactivity.google.com/myactivity).
On top of that, [take a look at your location history](https://www.google.com/maps/timeline).

Facebook is a major liability as well. They know your face,
where you live, where you have lived,
where your phone is, who your friends are -
often without you having to tell them. Their alogorithms
are built to stalk you on and off site. By reading their
privacy policy, one can find that Facebook [collects data
about you without you interacting with them](https://www.facebook.com/about/privacy).
They also demand that you not be anonymous, so pseudonyms
and alternative handles are not allowed. In some cases
it will ask you to provide legal identification for
verification.

![Social Media Icons on Cell Phone](https://burst.shopifycdn.com/photos/social-media-on-cell-phone_925x.jpg?attachment=social-media-on-cell-phone_925x.jpg)

## It's Not Just Their Websites

You might think, "okay, well I only use Facebook every once and
only use Google search. That's not dangerous, right?"
Unfortunately, that is not the case. Many websites use
things called [cookies](https://en.wikipedia.org/wiki/HTTP_cookie#Tracking)
to track your activity, following you across the sites
you've visited. They allow website developers to know
when, where, and for how long you visited a website -
something they have no business knowing for any site but
their own.

In addition, Google and Facebook have ways of tracking
people that aren't on their services. Google uses
[Google Analytics](https://en.wikipedia.org/wiki/Google_Analytics),
a platform used to gather data about
traffic on a website. According to Wikipedia, as of
2010, 49.95% of the top 1,000,000 websites used
Google Analytics. A chilling statistic to say the least.

Facebook's golden goose is their [like button plugin](https://en.wikipedia.org/wiki/Facebook_like_button#Tracking).
An expert from the Wikipedia article:

> The like button is implemented similarly to an advertising network,
> in that as more sites participate, Facebook is given a vast amount
> of information about who visits which websites and when. When loading
> a website that has the like button enabled, the user's web browser
> connects to Facebook's servers, which record which website was visited,
> and by what user.
>
> A week after the release of the social plugins, Facebook announced that
> 50,000 websites had installed the features, including the like button.
> Five months later, the number had increased to 2 million websites.

Another controversial bit is the Onava "VPN" application
that Facebook used to trick users into feeding them _all_
of the traffic from their mobile devices with the false
pretense of protecting their privacy.

## It's Closer Than You Think

The violations of your privacy go deeper than the web,
and deeper than Facebook and Google. [Apple and Microsoft
have both seen involvement](https://www.usatoday.com/story/news/2013/06/06/nsa-surveillance-internet-companies/2398345/)
with the [PRISM](https://en.wikipedia.org/wiki/PRISM_(surveillance_program)#Extent_of_the_program)
program alongside Facebook, Google, and Yahoo. Dropbox
as well.

Microsoft holds the largest market share when it comes
to desktop operating systems. They are also the authors
of the msot used office suites and one of the largest
email providers. According to the article on PRISM,
the NSA can request access to just about everything
on their servers, including things
like OneDrive, emails, or Word documents,
or [the data from everything
you type](https://privacy.microsoft.com/en-US/windows10privacy).

> If you turn on Speech, inking, & typing, **we collect samples
> of your typing and handwriting info** to improve our dictionaries
> and handwriting recognition for everybody who uses Windows.

Yeah...I wouldn't trust that they are _only_ using how they say.
Microsoft could also be using your data in a number of ways
they do not talk about, and most would be none the wiser
because you cannot view the code.

Microsoft is so determined to spy on you, their
even their open developer tools
[use](https://github.com/Microsoft/vscode/issues/16131)
 [telemetry](https://github.com/dotnet/cli/issues/3093)
 [by default](https://github.com/PowerShell/PowerShell/issues/6078),
with the only step towards properly respecting users
occuring in PowerShell.

Apple's cloud services and their involvement with PRISM
means macOS and iOS users are just as exposed.

## How Do I Protect Myself?

You might be feeling a little dejected at this point. What can one do
to stop being spied on if their privacy is being violated by everything?
Good news is, not everything spies on you. Below is a list of things to
help you start protecting yourself or your data:

* Do not use **_any_** services or software from the following companies:

    * Facebook
    * Google<sup>\*</sup>
    * Yahoo
    * Microsoft
    * Apple

* Do not trust any services within the U.S.A. or U.K. with sensitive data
([privacytools.io](https://www.privacytools.io/#ukusa))

* Use only [free, libre, and open-source software](https://www.gnu.org/philosophy/free-sw.html)

    * GNU/Linux instead of Windows or macOS (bold are recommended)
        * [Debian](https://www.debian.org/): For the perfect balance of free and easy
            * [**PureOS**](https://pureos.net/): A fully free distribution based on Debian,
            with security and privacy in mind
            * [Trisquel](https://trisquel.info/): A derivative of Debian with only free software.
            Slow updates.
            * [Ubuntu](https://www.ubuntu.com/): Easy setup, easy living. Most supported distribution
            by software vendors, includes proprietary software
        * [Fedora](https://getfedora.org/): Another well supported distro. Seperate package handling from
        Debian-based distributions
        * [Arch Linux](https://www.archlinux.org/): A flexible, extensible distribution good for learning
        about the innards of Linux. The community repository, the AUR, has most software that is not in the
        regular repositories. Quick software releases
            * [**Parabola GNU/Linux-libre**](https://parabola.nu): A derivative of Arch that takes freedom **very**
            seriously.
            * [**Hyperbola GNU/Linux-libre**](https://www.hyperbola.info/): The flexiblity of Arch with the stability of Debian,
            with only free software included.
            * [Manjaro](https://manjaro.org) or [Antergos](https://antergos.com/): Arch without the involved setup

    * Firefox or GNU Icecat instead of Chromium based browsers
    * [LineageOS](https://lineageos.org) (without Google Apps) or [Replicant](https://www.replicant.us/)
    instead of Android or iOS
    * [Matrix](https://matrix.org) via [Riot.im](https://about.riot.im/) for chat instead of Skype/WhatsApp/Messenger/Telegram/etc.
    * [LibreOffice](https://www.libreoffice.org/) instead of MS Office
    * [Krita](https://krita.org/en/) or [MyPaint](http://mypaint.org/) instead of SAI or other drawing software
    * Find a [Nextcloud](http://nextcloud.com/) instance instead of Dropbox, OneDrive, or iCloud
    * More at the [FSF directory](https://directory.fsf.org/wiki/Main_Page)

* Encrypt **everything**.

    * Start with the Free Software Foundation's [email self defense guide](https://emailselfdefense.fsf.org/en/)
    * Matrix has E2E encrypted chat, voice, and video calling supported through Riot
    * Learn about [Disk Encryption](https://wiki.archlinux.org/index.php/Disk_encryption)

* Stop searching from Google
    * [SearX](https://searx.me/): an [open-source](https://github.com/asciimoo/searx),
    [self-hostable](https://github.com/asciimoo/searx/wiki/Installation) metasearch engine
    * [DuckDuckGo](https://duckduckgo.com): A private, non-tracking search engine
        * [Privacy policy](https://duckduckgo.com/privacy)
        * [Why you can use them safely](https://security.stackexchange.com/questions/12664/why-would-someone-trust-duckduckgo-or-other-providers-with-a-similar-privacy-pol#)
        * Can be used with no JavaScript via <https://duckduckgo.com/html/> or <https://duckduckgo.com/lite/>
        * **Note**: They are U.S. based, so the government can manipulate them into making changes without
        disclosure
    * [Qwant](https://qwant.com): A France-based privacy-oriented search engine
        * [Privacy policy](https://about.qwant.com/legal/privacy/)

* Install the following (free software) extensions on Firefox:

    * [Privacy Badger](https://www.eff.org/privacybadger): Blocks trackers on websites you visit (e.g. Facebook like buttons, Google Analytics)
    * [Decentraleyes](https://decentraleyes.org/): Blocks request to CDNs (a possible tracking method) and provides locally stored
    versions of popular web frameworks and libraries
    * [uBlock Origin](https://github.com/gorhill/uBlock): A trustworthy, open adblocker
        * There is no official site outside of GitHub. Only the Firefox marketplace and the Chrome Store are trustworthy
        distributors. **DO NOT USE ublock.org**
    * [uMatrix **(For the vigilant. Highly Recommended)**](https://addons.mozilla.org/en-US/firefox/addon/umatrix/): In-depth configuration
    for content and script blocking. Helps stop tracking and cross-site attacks. Avoid sites that are completely non-functional without
    tweaks
    * [TOS;DR](https://tosdr.org/): A plugin providing short, informative summaries on the TOS of websites you visit
    * [Cookie Autodelete](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete): Automatically deletes cookies of visited sites
    either when you close the tab (**recommended**) or close the browser. Turn on auto-clean and whitelist as few sites as possible.

<sup>\* The only exception to this is free software with full source code available under a free
license. This allows for the [AOSP](https://source.android.com/) and open source ROMS based on it
to be used, but not Chrome or Chromium.</sup>

**You may use this content under the terms of the [CC-BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/2.0/) license**.
